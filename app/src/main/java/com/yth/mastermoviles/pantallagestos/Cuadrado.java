package com.yth.mastermoviles.pantallagestos;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by mastermoviles on 27/10/17.
 */

public class Cuadrado extends View {

    private static int DEFAULT_SIZE = 50;
    private int widthSize;
    private int heightSize;
    private float mX;
    private float mY;
    private float vX = 0;
    private float vY = 0;

    GestureDetectorCompat mDetectorGestos;
    boolean red = true;

    public Cuadrado(Context context) {
        super(context);
        ListenerGestos lg = new ListenerGestos();
        mDetectorGestos = new GestureDetectorCompat(context, lg);
    }

    public Cuadrado(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        ListenerGestos lg = new ListenerGestos();
        mDetectorGestos = new GestureDetectorCompat(context, lg);
    }

    public Cuadrado(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        ListenerGestos lg = new ListenerGestos();
        mDetectorGestos = new GestureDetectorCompat(context, lg);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public Cuadrado(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        ListenerGestos lg = new ListenerGestos();
        mDetectorGestos = new GestureDetectorCompat(context, lg);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Paint myPaint = new Paint();
        if (red) {
            myPaint.setColor(Color.RED);
        } else {
            myPaint.setColor(Color.BLUE);
        }
        myPaint.setStyle(Paint.Style.FILL);
        // canvas.drawRect(widthSize/2-25, heightSize/2-25, widthSize/2+25, heightSize/2+25, myPaint);
        canvas.drawRect(mX-DEFAULT_SIZE/2, mY-DEFAULT_SIZE/2, mX+DEFAULT_SIZE/2, mY+DEFAULT_SIZE/2, myPaint);

        myPaint.setColor(Color.GREEN);
        myPaint.setStyle(Paint.Style.STROKE);
        myPaint.setStrokeWidth(3);
        canvas.drawLine(mX, mY, mX + vX, mY + vY, myPaint);

        super.onDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width = DEFAULT_SIZE;
        int height = DEFAULT_SIZE;

        switch (widthMode) {
            case MeasureSpec.EXACTLY:
                width = widthSize;
                break;
            case MeasureSpec.AT_MOST:
                //if(width > widthSize)
                width = widthSize;
                break;
        }

        switch (heightMode) {
            case MeasureSpec.EXACTLY:
                height = heightSize;
                break;
            case MeasureSpec.AT_MOST:
                //if (height > heightSize)
                height = heightSize;
                break;
        }

        mX = widthSize/2;
        mY = heightSize/2;

        this.setMeasuredDimension(width,height);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mDetectorGestos.onTouchEvent(event);

        /*if(event.getAction() == MotionEvent.ACTION_DOWN) {
            return (event.getX() <= mX+DEFAULT_SIZE/2 && event.getX()>= mX-DEFAULT_SIZE/2 && event.getY() <= mY+DEFAULT_SIZE/2 && event.getY()>= mY-DEFAULT_SIZE/2);
        }
        if(event.getAction() == MotionEvent.ACTION_MOVE) {
            mX = event.getX();
            mY = event.getY();
            this.invalidate();
            return true;
        }
        return super.onTouchEvent(event);*/
    }

    class ListenerGestos extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent event) {
            return (event.getX() <= mX+DEFAULT_SIZE/2 && event.getX()>= mX-DEFAULT_SIZE/2 && event.getY() <= mY+DEFAULT_SIZE/2 && event.getY()>= mY-DEFAULT_SIZE/2);
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            mX = e2.getX();
            mY = e2.getY();
            vY = 0;
            vX = 0;
            invalidate();
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            red = !red;
            invalidate();
            return super.onSingleTapUp(e);
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            vX = velocityX;
            vY = velocityY;
            invalidate();
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }
}
